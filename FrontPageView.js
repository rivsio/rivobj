class FrontPageView{

  constructor(zone,callbackDetails,callbackChercher) {
    this.zone=zone;
    this.callbackDetails=callbackDetails;
    this.callbackChercher=callbackChercher;
  }

  viewMain = () =>`
  <h1 class="mb-4">Niveaux d'eau</h1>
  <h2 class="mb-4">Chercher une Rivière</h2>
  <!-- Search Bar -->
  <div className="mb-3">
  <label htmlFor="searchRiv" className="form-label">Rivière:</label>
  <input type="text" className="form-control" id="searchRiv" placeholder="Entrer une rivière">
  <br/>
  <label htmlFor="searchDpt" className="form-label">Département:</label>
  <input type="text" className="form-control" id="searchDpt" placeholder="Numéro du département">
  <br/>
  <label htmlFor="searchLoc" className="form-label">Localité:</label>
  <input type="text" className="form-control" id="searchLoc" placeholder="Localité de la station">
  <br/>
  <button id="btcherche">Chercher</button>
  </div>
  <!-- Liste des rivières -->
  <div className="row" id="riverList"></div>
  <h2 class="mb-4">Rivières suivies</h2>
  <div className="row" id="followedList"></div>
  `;

 fillSearchResults(data){
    const riverList = document.getElementById('riverList');
     riverList.innerHTML = '<h2 class="mb-4">Rivières trouvées</h2>';
     data.results.forEach(riviere => {
      let cont=document.createElement("div");
      riverList.appendChild(cont);
      let fv=new FoundView(cont,this.callbackDetails);
      console.log("trouvée:"+riviere.lbstationhydro);
      fv.fill(riviere);
    });
  }

  fill(){
    this.zone.innerHTML=this.viewMain();
    document.getElementById('btcherche').addEventListener('click', this.callbackChercher);
  }
}
