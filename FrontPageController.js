class FrontPageController{

  constructor (database){
    this.db=database;
    let zoneMain=document.getElementById("main");
    var obj=this;
    this.view=new FrontPageView(zoneMain,function(arg){obj.displayDetails(arg);},function(){obj.chercheRivieres();});
  }

  displayDetails(riviere){
    let dpc= new DetailsPageController(this.db);
    dpc.display(riviere);
  }

  displaySuivies(){
    var caller=this;
    const transaction=this.db.transaction("rivers","readwrite");
    const store=transaction.objectStore("rivers");
    let req=store.getAll();
    req.onsuccess=function (){
      let zoneFollowed=document.getElementById("followedList");
      req.result.forEach(result => {
        let cont=document.createElement("div");
        zoneFollowed.appendChild(cont);
        let fview=new FollowedView(cont,function(r,z){caller.enleverRiviereSuivie(r,z)});
        let cf=new ClientFacade();
        cf.modelFetchLevel(result.stationId,fview);
      });
    }
    req.onerror=function(){
      alert("Echec de la requete");
    }
  }

  display(){
    console.log("display this name in FPC"+this.constructor.name);
    this.view.fill();
    this.displaySuivies();
  }



  chercheRivieres(){
    let riv=document.getElementById('searchRiv').value.toLowerCase();
    let dpt=document.getElementById('searchDpt').value.toLowerCase();
    let loc=document.getElementById('searchLoc').value.toLowerCase();
    console.log("On cherche la riviere "+riv);
    let cf=new ClientFacade();
    cf.modelFetchLevels(riv,dpt,loc,this.view);
  }

  enleverRiviereSuivie(riv,zone){
    let id=riv.lbstationhydro;
    const transaction=this.db.transaction("rivers","readwrite");
    const store=transaction.objectStore("rivers");
    console.log("on enleve "+id);
    let request = store.delete(id);
    transaction.oncomplete = () => {
      zone.parentNode.removeChild(zone);
    };
  }

}
