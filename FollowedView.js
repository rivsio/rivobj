class FollowedView{


  constructor(zone,callbackSuppr) {
    this.zone=zone;
    this.callbackSuppr=callbackSuppr;
  }

  viewFollowed = (level) => `
  <div class="col-md-4 mb-4">
  <div class="card sresult">
  <div class="card-body">
  <h5 class="card-title">${level.lbstationhydro}</h5>
  <p class="card-text">Hauteur: ${level.hauteur} <br> Débit: ${level.debit}</p>
  <button class="remFolBtn">Enlever</button>
  </div>
  </div>
  </div>
  `;


  fill(data){
    let riviere=data.results[0];
    this.zone.innerHTML=this.zone.innerHTML+this.viewFollowed(riviere);
    let btn=this.zone.getElementsByClassName("remFolBtn")[0];
    btn.addEventListener('click', (event) => ((arg1,arg2) => {
      this.callbackSuppr(arg1,arg2);
    })(riviere,this.zone));
  }

}
