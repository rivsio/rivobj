class DetailsPageView{

  constructor(zone,callbackSuivre,callBackRetour) {
    this.zone=zone;
    this.callbackSuivre=callbackSuivre;
    this.callBackRetour=callBackRetour;
  }


 viewRiverDetails = (level) => `
      <h1 class="mb-4">Niveaux d'eau</h1>
      <h2 class="mb-4">Détail de la Rivière</h1>
      <div class="row">
      <div class="col-md-4 mb-4">
        <div class="card">
        <div class="card-body">
              <h5 class="card-title">${level.lbstationhydro}</h5>
        <p class="card-text">Hauteur: ${level.hauteur} <br> Débit: ${level.debit}</p>
        </div>
        </div>
      </div>
      </div>
      <div class="row">
      <div class="col-md-4 mb-4">
        <form>
        <label htmlFor="lmin" className="form-label">Niveau d'alerte minimal:</label>
        <input type="text" className="form-control" id="lmin" placeholder="Niveau d'alerte minimal">
        <label htmlFor="lmax" className="form-label">Niveau d'alerte maximal:</label>
        <input type="text" className="form-control" id="lmax" placeholder="Niveau d'alerte maximal">
        <br/>
        <button id="btsuivre">Suivre</button>
        <button id="btretour">Retour</button>
        </form>
      </div>
      </div>
      </div>
      `;

  fill(riviere){
    this.zone.innerHTML=this.viewRiverDetails(riviere);
    let bts=document.getElementById('btsuivre');
    let caller=this;
    bts.addEventListener('click', function(event){
      event.preventDefault();
      console.log("fill riviere caller is "+caller.constructor.name);
      caller.callbackSuivre();
    });
    let btr=document.getElementById('btretour');
    btr.addEventListener('click', function (event){
      event.preventDefault();
      caller.callBackRetour();
    });

  }

}
