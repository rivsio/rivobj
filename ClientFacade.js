class ClientFacade{

      async modelFetchLevels(riv,dpt,loc, view){
        await fetch("https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/vigicrues/records?select=dep_code%2C%20lbstationhydro%2C%20hauteur%2C%20debit%2Ctimestamp%20&where=dep_code%20%3D%20%27"+dpt+"%27%20and%20lbstationhydro%20like%20%20%27%25"+riv+"%20à%20"+loc+"%25%27&order_by=timestamp%20desc&limit=1")
                .then(response => response.json())
                .then(data => view.fillSearchResults(data))
                .catch(error => console.error('Error fetching niveau d\'eau:', error));
      }

      async modelFetchLevel(lbsta,view){
        await fetch('https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/vigicrues/records?select=dep_code%2C%20lbstationhydro%2C%20hauteur%2C%20debit%2Ctimestamp%20&where=lbstationhydro%20like%20%20%27%25'+lbsta.replaceAll("'", "\\'")+'%25%27&order_by=timestamp%20desc&limit=1')
                .then(response => response.json())
                .then(data => view.fill(data))
                .catch(error => console.error('Error fetching niveau d\'eau:', error));
      }
}
