function enableInstall(){
      console.log(`enabling btn`);
      let installPrompt = null;
    const installButton = document.querySelector("#install");
    window.addEventListener("beforeinstallprompt", (event) => {
  event.preventDefault();
  installPrompt = event;
  installButton.removeAttribute("hidden");
    });
    installButton.addEventListener("click", async () => {
  if (!installPrompt) {
      return;
  }
  const result = await installPrompt.prompt();
  console.log(`Install prompt was: ${result.outcome}`);
  disableInAppInstallPrompt();
    });
}

function init(){
  /****************service worker init****************************************/
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/~sam/niveaux/workers.js')
    .then(registration => {
      console.log('Service Worker enregistré avec succès.', registration);
    })
    .catch(error => {
      console.log("Erreur lors de l'enregistrement du Service Worker.", error);
    });
  }


  enableInstall();


  /****************db init and startup****************************************/
  let db;
  const request = indexedDB.open("niveauxdb");
  request.onerror = (event) => {
    console.error("Why didn't you allow my web app to use IndexedDB?!");
  };


  request.onupgradeneeded = (event) => {
    db = event.target.result;
    const store=db.createObjectStore("rivers",{ keyPath:"stationId"});
  };


  request.onsuccess = (event) => {
    db = event.target.result;
    let fpc=new FrontPageController(db);
    fpc.display();
  };
}
