class FoundView{

  constructor(zone,callbackDetails) {
    this.zone=zone;
    this.callbackDetails=callbackDetails;
  }

  viewFound = (riviere) => `
  <div class="col-md-4 mb-4">
  <div class="card sresult">
  <div class="card-body">
  <h5 class="card-title">${riviere.lbstationhydro}</h5>
  <p class="card-text">Hauteur: ${riviere.hauteur} <br> Débit: ${riviere.debit}</p>
  </div>
  </div>
  </div>
  `;

fill(data){
    let riviere=data;
    this.zone.innerHTML=this.viewFound(riviere);
    console.log("FoundView drawn");
    let zoneCard=this.zone.getElementsByClassName("sresult")[0];
    zoneCard.addEventListener('click', (event) => ((arg) => {
      this.callbackDetails(arg);
    })(riviere));
  }


}
