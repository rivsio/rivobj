
  class DetailsPageController{

    constructor(database){
      this.db=database;
    }

      display(riviere){
        let mainDiv= document.getElementById('main');
        let fpc=new FrontPageController(this.db);
        var obj=this;

        console.log("obj in display is one "+obj.constructor.name);
        let dpv=new DetailsPageView(mainDiv,function(){obj.suivreRiviere();},function(){fpc.display();});
        dpv.fill(riviere);
      }

      suivreRiviere(){
        let lbStationId=document.getElementsByClassName("card-title")[0].innerText;
        let lmin=document.getElementById("lmin").value;
        let lmax=document.getElementById("lmax").value;
        let transaction=this.db.transaction("rivers","readwrite");
        let store=transaction.objectStore("rivers");
        store.put({stationId: lbStationId , lmin: lmin, lmax: lmax} );
        alert("La rivière a été ajoutée à vos rivières suivies");
      }

}
